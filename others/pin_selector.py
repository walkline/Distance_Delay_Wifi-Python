from micropython import const

"""
					+----------------+
					|   Pin Define   |
					|                |
	  TOUT---ADC0---|  A0        D0  |---GPIO16---USER---WAKE
	     RESERVED---| RSV        D1  |---GPIO5
	     RESERVED---| RSV        D2  |---GPIO4
	SDD3---GPIO10---| SD3        D3  |---GPIO0---FLASH
	 SDD2---GPIO9---| SD2        D4  |---GPIO2---TXD1
	  SDD1---MOSI---| SD1        3V3 |---3.3V
	   SDCMD---CS---| CMD        GND |---GND
	  SDD0---MISO---| SD0        D5  |---GPIO14---HSCLK
	 SDCLK---SCLK---| CLK        D6  |---GPIO12---HMISO
			  GND---| GND        D7  |---GPIO13---RXD2---HMOSI
			 3.3V---| 3V3        D8  |---GPIO15---TXD2---HCS
			   EN---|  EN        D9  |---GPIO3---RXD0
			  RST---| RST        D10 |---GPIO1---TXD0
			  GND---| GND        GND |---GND
		   VIN 5V---| Vin        3V3 |---3.3V
					+----------------+
"""

class GPIO():
	D0 = GPIO16_ONLY_DIO = USER = WAKE = const(16)
	D1 = GPIO5 = const(5)
	D2 = GPIO4 = const(4)
	D4 = GPIO2 = TXD1 = LED = const(2)
	D5 = GPIO14 = HSCLK = const(14)
	D6 = GPIO12 = HMISO = const(12)
	D7 = GPIO13 = RXD2 = HMOSI = const(13)
	D8 = GPIO15 = TXD2 = HCS = const(5)
	D9 = GPIO3 = RXD0 = const(3)
	D10 = GPIO1 = TXD0 = const(1)
