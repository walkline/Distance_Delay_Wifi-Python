# 超声波测距模块控制继电器

## 项目介绍
使用 ESP8266 控制超声波测距模块和继电器模块 自动/手动/远程 控制小家电

## 物料说明
- MCU使用的是带有ESP8266模块的`NodeMCU`
- 超声波测距模块为`HC-SR04 模块`，驱动电压`5V`
- 继电器模块为`低电平触发`，驱动电压`5V`

## 使用说明
> 在上传代码文件到NodeMCU的时候，务必排除以下文件/文件夹，以节省存储空间

	.gitignore
	README.md
	/images
	/web/images

### 接线图
![接线图](images/01.png)

### 使用客户端访问NodeMCU模块
1. 按照接线示意图连接好所有模块
2. 给`NodeMCU模块`上电
3. 稍等片刻，蓝色Led灯会闪烁，表示Wifi热点已建立，等待`客户端`连接
4. 使用手机或电脑作为`客户端`连接Wifi热点，热点名称`Walkline`，密码`12345678`
5. `客户端`连接成功后蓝色Led灯会停止闪烁
6. 打开浏览器访问网址`http://192.168.4.1:8266`，网页内容如图所示
<div align=center><img width="550" alt="网页内容" src="./images/02.png"/></div>

> 如果使用串口工具连接`NodeMCU模块`，会显示如下调试信息

	Wifi access point intialized:
	    ssid: Walkline
	    password: 12345678
	
	Server started and listening on ('0.0.0.0', 8266)
	
	client connected from ('192.168.4.2', 47566)
	
	- client request: GET / HTTP/1.1
	- request file: /
	-- file name: /web/index.html
	-- file size: 4697
	- send response header
	- send response file
	- file send ok
	client connection closed manually
	
	client connected from ('192.168.4.2', 47569)
	
	- client request: GET /js/main.js HTTP/1.1
	- request file: /js/main.js
	-- file name: /web/js/main.js
	-- file size: 18927
	- send response header
	- send response file
	- file send ok
	client connection closed manually
	
	client connected from ('192.168.4.2', 47572)
	
	- client request: GET /?query_command=get_status HTTP/1.1
	- send js response header
	- send js response data
	- js response send ok
	client connection closed manually

### 使用客户端控制NodeMCU模块
控制`NodeMCU模块`主要是使用它控制超声波测距模块和继电器模块，实现`远程 / 自动`控制继电器模块的目的

#### 远程控制
直接点击页面中的`Turn On / Turn Off`按钮，就可以实现远程控制继电器

#### 自动控制
点击页面中的`Auto`按钮，直到`Manual`按钮变为蓝色，就可以实现自动控制继电器
- 当人体靠近超声波测距模块30cm位置时，等待约2秒钟，继电器自动激活工作
- 当人体离开后，等待约3秒钟，继电器将停止工作

### 手动控制NodeMCU模块
直接使用面包板上的按钮，就可以实现手动控制继电器