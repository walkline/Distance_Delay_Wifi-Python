class SocketServiceResponseBuilder:
	def __init__(self):
		self._body = ""
		self._result_status = ""
		self._content_type = "Content-Type: text/html; charset=UTF-8"
		self._server_name = "Server: ESP8266 Server"
		self._connection = "Connection: keey-alive"
		self._accept_ranges = "Accept-Ranges: bytes"
		self._content_length = ""

	def set_result_status(self, ok: bool = True):
		if (ok):
			self._result_status = "HTTP/1.1 200 OK"
		else:
			self._result_status= "HTTP/1.1 404 File not found"

	def set_content_type(self, content_type: str):
		self._content_type = "Content-Type: " + content_type

	def set_server_name(self, server_name: str):
		self._server_name = server_name

	def set_connection(self, connection):
		self._connection = connection

	def set_content_length(self, content_length):
		self._content_length = "Content-Length:" + str(content_length)

	def set_body_data(self, body_data: str):
		self._body += body_data

	def generate_response_header(self):
		response_header = self._result_status + "\r\n"
		response_header += self._content_type + "\r\n"
		response_header += self._server_name + "\r\n"
		response_header += self._accept_ranges + "\r\n"
		response_header += self._connection + "\r\n"
		response_header += self._content_length + "\r\n"
		response_header += "\r\n"

		return response_header