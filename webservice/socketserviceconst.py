class SocketServiceConst:
	RESPOND_FOR_CLIENT_README = """\
Command List:
	激活继电器："/?cmd=active_relay"
	关闭继电器："/?cmd=deactive_relay"
	启用继电器自动模式："/?cmd=enable_auto_relay"
	禁用继电器自动模式："/?cmd=disable_auto_relay"
	获取当前模式："/?cmd=query_distance_status"
"""

	RESPOND_FOR_WEB_BROWSER_NORMAL = """\
HTTP/1.1 200 OK
Content-Type: text/html; charset=UTF-8
Server: ESP8266 Server
Content-Length: 80
Connection: keep-alive

<html><head><title>esp</title></head><body><h1>ESP8266 Server</h1></body></html>
"""

	COMMAND_ACTIVE_RELAY = "/?cmd=active_relay"
	COMMAND_DEACTIVE_RELAY = "/?cmd=deactive_relay"
	COMMAND_ENABLE_AUTO_RELAY = "/?cmd=enable_auto_relay"
	COMMAND_DISABLE_AUTO_RELAY = "/?cmd=disable_auto_relay"
	COMMAND_QUERY_DISTANCE_STATUS = "/?cmd=query_distance_status"

	WEB_ROOT = "/web"
